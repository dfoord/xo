(() => {
        
  'use strict';
  
  //TODO: make program callback based with logical steps to execute on each turn and startup
  var CONTENT = document.getElementById('content-wrapper'),
    GRID,    //the object containing the game state
    PLAYER,  //The current player to act
    WINNER,  //The round winner 
    BLOCKS,
    MOVE_COUNT; 
  
  function setGlobals() {
    GRID = [
      [null, null, null],
      [null, null, null],
      [null, null, null]
    ];
    PLAYER = 1;
    WINNER = null;
    MOVE_COUNT = 0;
  }
  
  function initialize() {
    setGlobals();
    createBlocks(blocks => {
      BLOCKS = blocks;
      bindEventListeners(blocks);
    });
    setTurnIndicator();
  }
  
  function setTurnIndicator() {
    var indicator = document.getElementById('turn_indicator');
    indicator.innerHTML = '<i class="fa fa-user"></i>&nbsp;' + PLAYER + '\'s&nbsp;turn';
  }
  
  function alternateTurn() {
    switch (PLAYER) {
      case 1: 
        PLAYER = 2;
        break;
      case 2:
        PLAYER = 1;
        break;
    }
    setTurnIndicator();
  }

  function createBlocks(done) {
    for(var i = 0; i < GRID.length; i++) { // x-axis loop
      for(var j = 0; j < GRID[i].length; j++) { //y-axis loop

        //Create new block and smash it in the game state array
        var block = document.createElement('div');
        block.id = i + '_' + j;
        block.className = 'block';
        block.attributes.y = j;
        block.attributes.x = i;
        CONTENT.appendChild(block);

        //break a line on every third block on the x-axis
        if(j === 2){
          CONTENT.appendChild(document.createElement('br'));
        }
      }
    }
    
    var blocks = document.getElementsByClassName('block');
    done(blocks);
  }
  
  function resetBlocks(blocks) {
    for(var i = 0; i < blocks.length; i++) { //iterate through the game blocks
      var block = blocks[i];
      block.textContent = null;
      block.className = 'block';
    }
  }

  function bindEventListeners(blocks) {
    for(var i = 0; i < blocks.length; i++) { //iterate through the game blocks
      blocks[i].onclick = event => { //bind an anonymous function to the click event
       
        var clicked = event.target;
        var attrs = clicked.attributes;
       
        if(GRID[attrs.x][attrs.y] === null) { //check if the block has been clicked
          
          GRID[attrs.x][attrs.y] = PLAYER; //assign the block to a player
          MOVE_COUNT ++;
          
          //color the block and alternate the player turn
          setButtonText(clicked, () => {
            computeResults(GRID, win => {
              if (win === true) {
                displayOverlay(PLAYER);
              } else if(MOVE_COUNT === 9) {
                displayOverlay(null);
              } else {
                alternateTurn();
              }
            });
          });
          
        }
      };
    }
  }
  
  function setButtonText(button, done) {
    if(PLAYER === 1){
      button.textContent = 'X';
      button.className += ' player_one';
    } else {
      button.textContent = 'O'
      button.className += ' player_two';
    }
    done();
  }

  function computeResults(map, done) {
 
    var hasYwinningCombo = (() => {
      for(var i = 0; i < map.length; i++) {
       if(map[0][i] !== null){
         if(map[0][i] === map[1][i] && map[1][i] === map[2][i]) {
          //Set winning button classname
          for(var y = 0; y < 3; y++) {
            document.getElementById(y + '_' + i).className += ' winning';
          }
          return true;
        }
       }
      }
    })();
    
    var hasXwinningCombo = (() => {
      for(var i = 0; i < map.length; i++) {
        if(map[i][0] !== null){
          if(map[i][0] === map[i][1] && map[i][1] === map[i][2]) {
            //Set winning button classname
            for(var x = 0; x < 3; x++) {
              document.getElementById(i + '_' + x).className += ' winning';
            }
            return true;
          }
        } 
      }
    })();
    
    var hasDiagWinningCombo = (() => {
      if(map[1][1] !== null) {
        if(map[0][0] === map[1][1] && map[1][1] === map[2][2]) { 
          //Set winning button classname
          for(var i = 0; i < 3; i++) {
            document.getElementById(i + '_' + i).className += ' winning';
          }
          return true;
        } else if(map[0][2] === map[1][1] && map[1][1] === map[2][0]) {
          //Set winning button classname
          for(var i = 0 ; i < 3; i++) {
            document.getElementById(i + '_' + (2 - i)).className += ' winning';
          }
          return true;
        }
      }
      return false;
    })();
    
    if(hasXwinningCombo | hasYwinningCombo | hasDiagWinningCombo) {
      done.call(null, true);
    } else {
      done.call(null, false);
    }
  }
  
  //display winner overlay
  function displayOverlay(winner) {
    var overlay = document.createElement('div');
    overlay.id = 'overlay';
    
    var overlayContent = document.createElement('div');
    overlayContent.id = 'overlay-content';
    
    var resetButton = document.createElement('button');
    resetButton.id = 'resetButton';
    resetButton.innerHTML = '<i class="fa fa-reply"></i>&nbsp;Play Again';
    resetButton.className = 'button button-default';
    resetButton.onclick = reset;
    
    var heading = document.createElement('h1');
    if(winner !== null) {
      heading.innerHTML = 'Player ' + winner + ' Wins !';
    } else {
      heading.innerHTML = 'It\'s a Draw :(';
    }
    
    var indicator = document.getElementById('turn_indicator');
    indicator.innerHTML = 'Game Over';
    
    overlayContent.appendChild(resetButton);
    overlayContent.appendChild(heading);
    overlayContent.appendChild(resetButton);
    overlay.appendChild(overlayContent);
    document.body.insertBefore(overlay, CONTENT);
  }
  
  function reset() {
    document.body.removeChild(document.getElementById('overlay')); //remove the overlay from the DOM
    setGlobals();
    resetBlocks(BLOCKS);
    setTurnIndicator();
  }
  
  //initialize the game
  initialize();
  
})();