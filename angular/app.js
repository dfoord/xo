'use strict';

angular.module('xo', [])
  .constant('$', $)
  .directive('fadeIn', function($) {
    return {
      link: function(scope, elem, attrs) {
        $(elem).fadeIn();
      }
    };
  })
  .directive('overlay', function($) {
    return {
      scope:{
        visible: '='
      },
      link: function(scope, elem, attrs) {
        $(elem).context.style.display = 'none';
        scope.$watch('visible', function(newVal) {
          if(newVal === true) {
            $(elem).fadeIn();
          } else {
            $(elem).fadeOut();
          }
        });
      }
    };
  })
  .controller('GameCtrl', function($scope) {
    
    $scope.overlay = {};
    
    $scope.init = function(){
      $scope.game = {
        blocks: [
          [{x:0, y:0}, {x:1, y:0}, {x:2, y:0}],
          [{x:0, y:1}, {x:1, y:1}, {x:2, y:1}],
          [{x:0, y:2}, {x:1, y:2}, {x:2, y:2}]
        ],
        turnNo: 0,
        activePlayer: 1,
        over: false
      };
    };
    
    $scope.reset = function() {
      $scope.init();
    };
    
    $scope.takeBlock = function(block) {
      if(block.p === undefined) {
        $scope.game.turnNo ++;
        markBlock(block, function() {
          computeWin(function(win) {
            if(win) {
              $scope.overlay.message = 'Player ' + $scope.game.winner + ' Wins !';
            } else if ($scope.game.turnNo === 9) {
              $scope.overlay.message = 'It\'s a Draw :(';
              $scope.game.over = true;
            }
          });
        });
      }
    };
    
    var markBlock = function(block, done){
      if($scope.game.activePlayer === 1) {
        block.p = 1;
        block.txt = 'X';
        $scope.game.activePlayer = 2;
      } else {
        block.p = 2;
        block.txt = 'O';
        $scope.game.activePlayer = 1;
      }
      done.call(this);
    };
    
    var computeWin = function(done) {
      
      let b = $scope.game.blocks;
      
      let hasXwinningCombo = (function () {
        for(let x = 0; x < b.length; x++) {
          if(b[x][0].p !== undefined){
            if(b[x][0].p === b[x][1].p && b[x][1].p === b[x][2].p) {
              $scope.game.winner = b[x][0].p;
              for(let y = 0; y < 3; y++) {
               b[x][y].winner = true;
              }
              return true;
            }
          } 
        }
      })();
      
      let hasYwinningCombo = (function () {
        for(let y = 0; y < b.length; y++) {
          if(b[0][y].p !== undefined){
            if(b[0][y].p === b[1][y].p && b[1][y].p === b[2][y].p) {
              $scope.game.winner = b[0][y].p;
              for(let x = 0; x < 3; x++) {
               b[x][y].winner = true;
              }
              return true;
            }
          } 
        }
      })();
      
      let hasDiagWinningCombo = (function () {
        if(b[1][1].p !== undefined) {
          if(b[0][0].p === b[1][1].p && b[1][1].p === b[2][2].p) { 
            $scope.game.winner = b[0][0].p;
            for(let i = 0; i < 3; i++) {
               b[i][i].winner = true;
            }
            return true;
          } else if(b[0][2].p === b[1][1].p && b[1][1].p === b[2][0].p) {
            $scope.game.winner = b[0][2].p;
            for(let i = 0 ; i < 3; i++) {
              b[i][(2 - i)].winner = true; 
            }
            return true;
          }
        }
      })();
      
      if(hasXwinningCombo | hasYwinningCombo | hasDiagWinningCombo) {
        $scope.game.over = true;
        done.call(this, true);
      } else {
        done.call(this, false);
      }
      
    };
     
  });