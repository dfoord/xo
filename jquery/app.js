(function($){
        
  'use strict';
  
  var GRID,     //the object containing the game state
    PLAYER,     //The current player to act
    WINNER,     //The round winner 
    MOVE_COUNT  //The amount of moves made in the current game; 
  
  /**
  * Sets the Global Variables to the default state
  */
  function setGlobals() {
    GRID = [
      [null, null, null],
      [null, null, null],
      [null, null, null]
    ];
    PLAYER = 1;
    WINNER = null;
    MOVE_COUNT = 0;
  }
  
  function initialize() {
    setGlobals();
    createBlocks(function() {
      bindEventListeners();
      $('#content-wrapper').fadeIn();
    });
    setTurnIndicator();
  }
  
  function setTurnIndicator() {
    $('#turn_indicator').html('Player ' + PLAYER + '\'s turn');
  }
  
  function alternateTurn() {
    switch (PLAYER) {
      case 1: 
        PLAYER = 2;
        break;
      case 2:
        PLAYER = 1;
        break;
    }
    setTurnIndicator();
  }

  function createBlocks(done) {
    for(let i = 0; i < GRID.length; i++) { // x-axis loop
      for(let j = 0; j < GRID[i].length; j++) { //y-axis loop

        //Create new block and smash it in the game state array
        let block = "<div class='block' id='" + i + "_" + j + "' x='" + i + "' y='" + j + "'></div>";
        $('#content-wrapper').append(block);

        //break a line on every third block on the x-axis
        if(j === 2){
          $('#content-wrapper').append('<br/>');
        }
      }
    }
    done();
  }
  
  function resetBlocks() {
    $('.block').html('');
    $('.block').removeClass('winning');
    $('.block').removeClass('player_one');
    $('.block').removeClass('player_two');
  }

  function bindEventListeners() {
    $('.block').click(function(){
      
      let self = $(this);
      let x = self.attr('x');
      let y = self.attr('y');
    
      if(GRID[x][y] === null) { //check if the block has been clicked
        
        GRID[x][y] = PLAYER; //assign the block to a player
        MOVE_COUNT ++;
        
        //color the block and alternate the player turn
        setButtonText(self, function(){
          computeWin(GRID, function(win) {
            if (win === true) {
              displayOverlay(PLAYER);
            } else if(MOVE_COUNT === 9) {
              displayOverlay(null);
            } else {
              alternateTurn();
            }
          });
        });  
      }
    });
  }
  
  function setButtonText(button, done) {
    if(PLAYER === 1){
      button.html('X');
      button.addClass('player_one');
    } else {
      button.html('O');
      button.addClass('player_two');
    }
    done.call(this);
  }

  function computeWin(map, done) {
 
    let hasYwinningCombo = (function () {
      for(let i = 0; i < map.length; i++) {
       if(map[0][i] !== null){
         if(map[0][i] === map[1][i] && map[1][i] === map[2][i]) {
          for(let x = 0; x < 3; x++) {
            $('#' + x + '_' + i).addClass('winning');
          }
          return true;
        }
       }
      }
    })();
    
    let hasXwinningCombo = (function () {
      for(let i = 0; i < map.length; i++) {
        if(map[i][0] !== null){
          if(map[i][0] === map[i][1] && map[i][1] === map[i][2]) {
            for(let y = 0; y < 3; y++) {
              $('#' + i + '_' + y).addClass('winning');
            }
            return true;
          }
        } 
      }
    })();
    
    let hasDiagWinningCombo = (function () {
      if(map[1][1] !== null) {
        if(map[0][0] === map[1][1] && map[1][1] === map[2][2]) { 
          for(let i = 0; i < 3; i++) {
            $('#' + i + '_' + i).addClass('winning');
          }
          return true;
        } else if(map[0][2] === map[1][1] && map[1][1] === map[2][0]) {
          for(let i = 0 ; i < 3; i++) {
            $('#' + i + '_' + (2 - i)).addClass('winning');
          }
          return true;
        }
      }
      return false;
    })();
    
    if(hasXwinningCombo | hasYwinningCombo | hasDiagWinningCombo) {
      done.call(this, true);
    } else {
      done.call(this, false);
    }
  }
  
  //display winner overlay 
  function displayOverlay(winner) {
    let overlay = '<div id="overlay" style="display:none"></div>';
    let overlayContent = '<div id="overlay-content"></div>'  
    let resetButton = '<button type="button" class="button button-default" id="btnReset" ><i class="fa fa-reply"></i>&nbsp;Play Again</button>';
    
    let heading = '';
    if(winner !== null) {
      heading = '<h1>Player ' + winner + ' Wins !</h1>';
    } else {
      heading = '<h1>It\'s a Draw :(</h1>';
    }
   
    $('body').prepend(overlay);
    $('#overlay').append(overlayContent);
    $('#overlay-content').append(heading);
    $('#overlay-content').append(resetButton);
    $('#overlay').fadeIn();
    $('#btnReset').click(reset);
    $('#turn_indicator').html('Game Over');
  }
  
  function reset() {
    $('#overlay').fadeOut();
    setTimeout(function(){
      $('#overlay').detach();
    }, 500);
    setGlobals();
    resetBlocks();
    setTurnIndicator();
  }
  
  //initialize the game
  initialize();
  
})(jQuery);